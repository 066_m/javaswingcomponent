import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;

public class ButtonEx {
    public static void main(String[] args) {
        JFrame frame = new JFrame("Button Example");

        JButton btn = new JButton("Click Here");
        btn.setBounds(750, 300, 95, 30);
        frame.add(btn);

        JButton btnI = new JButton(new ImageIcon("D:\\icecream-kawaii-sweet-cute-food-icon.png"));
        btnI.setBounds(150, 150, 500, 443);
        frame.add(btnI);


        frame.setSize(1000, 1000);
        frame.setLayout(null);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
    }
}
