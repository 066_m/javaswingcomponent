import java.awt.event.*;
import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.JFrame;

public class ButtonExAct {
    public static void main(String[] args) {
        JFrame frame = new JFrame("Button Example"); 

        final JTextField txt = new JTextField();  
        txt.setBounds(50,50, 150,20);  
        frame.add(txt);

        JButton btn = new JButton("Click Here");  
        btn.setBounds(50,100,95,30);  
        frame.add(btn);
        btn.addActionListener(new ActionListener(){  
            public void actionPerformed(ActionEvent e){  
                    txt.setText("Welcome to Dektuaklom.");  
                }  
        });  
          
        frame.setSize(400,400);  
        frame.setLayout(null);  
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true); 
    }
}
