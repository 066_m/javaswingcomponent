import java.awt.event.*;
import javax.swing.*;

public class CheckBoxEx {
    CheckBoxEx(){
        JFrame frame = new JFrame("CheckBox Example");
        
        final JLabel label = new JLabel();
        label.setHorizontalAlignment(JLabel.CENTER);
        label.setSize(400, 100);
        frame.add(label);
        
        JCheckBox chBox1 = new JCheckBox("C++");
        chBox1.setBounds(150, 100, 50, 50);
        frame.add(chBox1);

        chBox1.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                label.setText("C++ CheckBox: "
                + (e.getStateChange() == 1?"checked":"unchecked"));
            }
        });

        //JCheckBox chBox2 = new JCheckBox("Java", true);
        JCheckBox chBox2 = new JCheckBox("Java");
        chBox2.setBounds(150, 150, 100, 50);
        frame.add(chBox2);

        chBox2.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                label.setText("Java CheckBox: "
                + (e.getStateChange() == 1?"checked":"unchecked"));
            }
        });

        frame.setSize(400, 400);
        frame.setLayout(null);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
    }

    public static void main(String[] args) {
        new CheckBoxEx();
    }
}
