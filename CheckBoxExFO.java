import java.awt.event.*;
import javax.swing.*;

public class CheckBoxExFO extends JFrame implements ActionListener {
    JLabel lbl;
    JCheckBox chBox1, chBox2, chBox3;
    JButton btn;

    CheckBoxExFO(){
        lbl = new JLabel("Food Ordering System");
        lbl.setBounds(50, 50, 300, 20);
        add(lbl);

        chBox1 = new JCheckBox("Pizza @ 100");
        chBox1.setBounds(100, 100, 150, 20);
        add(chBox1);

        chBox2 = new JCheckBox("Burger @ 30");
        chBox2.setBounds(100, 150, 150, 20);
        add(chBox2);
        
        chBox3 = new JCheckBox("Tea @ 10");
        chBox3.setBounds(100, 200, 150, 20);
        add(chBox3);

        btn = new JButton("Order");
        btn.setBounds(100, 250, 80, 30);
        btn.addActionListener(this);
        add(btn);

        setSize(400, 400);
        setLayout(null);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        int amount = 0;
        String msg = "";
        if(chBox1.isSelected()){
            amount += 100;
            msg = "Pizza : 100\n";
        }
        if(chBox2.isSelected()){
            amount += 30;
            msg += "Burger : 30\n";
        }
        if(chBox3.isSelected()){
            amount += 10;
            msg += "Tea : 10\n";
        }
        msg += "------------\n";
        JOptionPane.showMessageDialog(this, msg + "Total : " + amount);
    }

    public static void main(String[] args) {
        new CheckBoxExFO();
    }
}
