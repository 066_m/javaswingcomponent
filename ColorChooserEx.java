import java.awt.event.*;
import java.awt.*;
import javax.swing.*;

public class ColorChooserEx extends JFrame implements ActionListener {
    JButton btn;
    Container ctn;

    ColorChooserEx() {
        ctn = getContentPane();
        ctn.setLayout(new FlowLayout());

        btn = new JButton("color");
        btn.addActionListener(this);
        ctn.add(btn);
    }

    public void actionPerformed(ActionEvent e) {
        Color initialcolor = Color.RED;
        Color color = JColorChooser.showDialog(this, "Select a color", initialcolor);
        ctn.setBackground(color);
    }

    public static void main(String[] args) {
        ColorChooserEx ch = new ColorChooserEx();
        ch.setSize(400, 400);
        ch.setVisible(true);
        ch.setDefaultCloseOperation(EXIT_ON_CLOSE);
    }

}
