import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class ColorChooserEx2 extends JFrame implements ActionListener {
    JFrame frame;
    JButton btn;
    JTextArea ta;

    ColorChooserEx2(){  
        frame = new JFrame("Color Chooser Example.");  

        btn = new JButton("Pad Color");  
        btn.setBounds(200,250,100,30);  
        btn.addActionListener(this);
        frame.add(btn);
        
        ta = new JTextArea();  
        ta.setBounds(10,10,300,200);  
        frame.add(ta);
         
        frame.setLayout(null);  
        frame.setSize(400,400);  
        frame.setVisible(true);  
    }

    public void actionPerformed(ActionEvent e) {
        Color c = JColorChooser.showDialog(this, "Choose", Color.CYAN);
        ta.setBackground(c);
    }

    public static void main(String[] args) {
        new ColorChooserEx2();
    }

}
