import java.awt.event.*;
import javax.swing.*;

public class ComboBoxEx {
    JFrame frame;

    ComboBoxEx(){
        frame = new JFrame("ComboBox Example");

        final JLabel label = new JLabel();
        label.setHorizontalAlignment(JLabel.CENTER);
        label.setSize(400, 100);
        frame.add(label);

        JButton btn = new JButton("Show");
        btn.setBounds(200, 100, 75, 20);
        frame.add(btn);

        String languages[] = {"C", "C++", "C#", "Java", "PHP"};
        
        final JComboBox cmBox = new JComboBox<>(languages);
        cmBox.setBounds(50, 100, 90, 20);
        frame.add(cmBox);

        btn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String data = "Programming language Selected : " 
                + cmBox.getItemAt(cmBox.getSelectedIndex());
                label.setText(data);
            }
        });

        frame.setSize(350, 350);
        frame.setLayout(null);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
    }

    public static void main(String[] args) {
        new ComboBoxEx();
    }
}
