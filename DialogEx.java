import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class DialogEx {
    private static JDialog dlg;

    DialogEx() {
        JFrame frame = new JFrame();
        dlg = new JDialog(frame, "Dialog Example", true);
        dlg.setLayout(new FlowLayout());

        dlg.add(new JLabel("Click button to continue."));

        JButton btn = new JButton("OK");
        btn.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                DialogEx.dlg.setVisible(false);
            }
        });
        dlg.add(btn);
        
        dlg.setSize(300, 300);
        dlg.setVisible(true);
    }

    public static void main(String args[]) {
        new DialogEx();
    }
}
