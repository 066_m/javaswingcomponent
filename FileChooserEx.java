import javax.swing.*;
import java.awt.event.*;
import java.io.*;

public class FileChooserEx extends JFrame implements ActionListener {
    JMenuBar mb;
    JMenu file;
    JMenuItem open;
    JTextArea ta;

    FileChooserEx(){   
        mb = new JMenuBar();    
        mb.setBounds(0,0,800,20);    
        add(mb);  

        file = new JMenu("File");   
        mb.add(file);

        open = new JMenuItem("Open File");    
        open.addActionListener(this);    
        file.add(open);    

        ta = new JTextArea(800,800);    
        ta.setBounds(0,20,800,800);                       
        add(ta);              
    }

    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == open) {
            JFileChooser fc = new JFileChooser();
            int i = fc.showOpenDialog(this);

            if (i == JFileChooser.APPROVE_OPTION) {
                File f = fc.getSelectedFile();
                String filepath = f.getPath();
                try {
                    BufferedReader br = new BufferedReader(new FileReader(filepath));
                    String s1 = "", s2 = "";
                    while ((s1 = br.readLine()) != null) {
                        s2 += s1 + "\n";
                    }
                    ta.setText(s2);
                    br.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }
    }

    public static void main(String[] args) {
        FileChooserEx om = new FileChooserEx();
        om.setSize(500, 500);
        om.setDefaultCloseOperation(EXIT_ON_CLOSE);
        om.setLayout(null);
        om.setVisible(true);
        
    }
}
