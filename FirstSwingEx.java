import javax.swing.JButton;
import javax.swing.JFrame;

public class FirstSwingEx {
    public static void main(String[] args) {
        JFrame frame = new JFrame();
        JButton btn = new JButton("click");
        btn.setBounds(130, 100, 100, 40);
        
        frame.add(btn);
        frame.setSize(400,500);
        frame.setLayout(null);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
    }
}
