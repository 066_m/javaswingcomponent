import javax.swing.JButton;
import javax.swing.JFrame;

public class FirstSwingExInh extends JFrame {
    JFrame frame;
    FirstSwingExInh(){
        JButton btn = new JButton("click");
        btn.setBounds(130, 100, 100, 40);

        add(btn);
        setSize(400, 500);
        setLayout(null);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);
    }

    public static void main(String[] args) {
        new FirstSwingExInh();
    }
}
