import java.awt.event.*;
import java.awt.*;
import javax.swing.*;

public class LabelEx extends JFrame implements ActionListener {
    JTextField txt;
    JLabel lbl;
    JButton btn;

    LabelEx(){
        txt = new JTextField();
        txt.setBounds(50, 50, 150, 20);
        add(txt);

        lbl = new JLabel("IP of this!?");
        lbl.setBounds(50, 100, 250, 20);
        add(lbl);

        btn = new JButton("Find IP");
        btn.setBounds(50, 150, 95, 30);
        btn.addActionListener(this);
        add(btn);

        setSize(500, 500);
        setLayout(null);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);
    }

    public void actionPerformed(ActionEvent e){
        try {
            String host = txt.getText();
            String ip = java.net.InetAddress.getByName(host).getHostAddress();
            lbl.setText("IP of " + host + " is : " + ip);
        } catch (Exception ex) {
            System.out.println(ex);
        }
    }

    public static void main(String[] args) {
        new LabelEx();
    }
}
