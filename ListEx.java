import java.awt.event.*;
import javax.swing.*;

public class ListEx {
    ListEx(){  
        JFrame frame = new JFrame(); 

        final JLabel label = new JLabel();          
        label.setSize(500,100);  
        frame.add(label);
        
        JButton btn = new JButton("Show");  
        btn.setBounds(200,150,80,30); 
        frame.add(btn);

        DefaultListModel<String> list1 = new DefaultListModel<>();  
        list1.addElement("C");  
        list1.addElement("C++");  
        list1.addElement("Java");  
        list1.addElement("PHP");  

        final JList<String> list_1 = new JList<>(list1);  
        list_1.setBounds(100,100, 75,75);  
        frame.add(list_1);  

        DefaultListModel<String> list2 = new DefaultListModel<>();  
        list2.addElement("Turbo C++");  
        list2.addElement("Struts");  
        list2.addElement("Spring");  
        list2.addElement("YII");  

        final JList<String> list_2 = new JList<>(list2);  
        list_2.setBounds(100,200, 75,75);  
        frame.add(list_2);

        btn.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String data = "";
                if (list_1.getSelectedIndex() != -1) {
                    data = "Programming language Selected: " + list_1.getSelectedValue();
                    label.setText(data);
                }
                if (list_2.getSelectedIndex() != -1) {
                    data += ", FrameWork Selected: ";
                    for (Object frame1 : list_2.getSelectedValuesList()) {
                        data += frame1 + " ";
                    }
                }
                label.setText(data);
            }
        });

        frame.setSize(500,500);  
        frame.setLayout(null);  
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);  
    }

    public static void main(String args[]) {
        new ListEx();
    }
}
