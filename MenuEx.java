import javax.swing.*;

public class MenuEx {
    JMenu menu, submenu;
    JMenuItem i1, i2, i3, i4, i5;

    MenuEx() {
        JFrame frame = new JFrame("Menu and MenuItem Example");
        JMenuBar mb = new JMenuBar();

        menu = new JMenu("Menu");
        i1 = new JMenuItem("Item 1");
        menu.add(i1);
        i2 = new JMenuItem("Item 2");
        menu.add(i2);
        i3 = new JMenuItem("Item 3");
        menu.add(i3);

        mb.add(menu);

        submenu = new JMenu("Sub Menu");
        i4 = new JMenuItem("Item 4");
        submenu.add(i4);
        i5 = new JMenuItem("Item 5");
        submenu.add(i5);

        menu.add(submenu);

        frame.setJMenuBar(mb);
        frame.setSize(400, 400);
        frame.setLayout(null);
        frame.setVisible(true);
    }

    public static void main(String args[]) {
        new MenuEx();
    }
}
