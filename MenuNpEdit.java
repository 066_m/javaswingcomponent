import java.awt.event.*;
import javax.swing.*;

public class MenuNpEdit implements ActionListener {
    JFrame frame;
    JMenuBar mb;
    JMenu file, edit, help;
    JMenuItem cut, copy, paste, selectAll;
    JTextArea ta;

    MenuNpEdit(){    
        frame = new JFrame();   

        mb = new JMenuBar();  
        frame.add(mb);   

        file = new JMenu("File"); 
        mb.add(file);   
        edit = new JMenu("Edit");   
        mb.add(edit);
        help = new JMenu("Help");         
        mb.add(help); 

        cut = new JMenuItem("cut"); 
        cut.addActionListener(this);  
        edit.add(cut);

        copy = new JMenuItem("copy");    
        copy.addActionListener(this);   
        edit.add(copy);

        paste = new JMenuItem("paste");   
        paste.addActionListener(this); 
        edit.add(paste);

        selectAll = new JMenuItem("selectAll");    
        selectAll.addActionListener(this);  
        edit.add(selectAll);
   
        ta = new JTextArea();    
        ta.setBounds(5,5,360,320);    
        frame.add(ta);
   
        frame.setJMenuBar(mb);  
        frame.setLayout(null);    
        frame.setSize(400,400);   
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); 
        frame.setVisible(true);    
    }

    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == cut)        ta.cut();
        if (e.getSource() == paste)      ta.paste();
        if (e.getSource() == copy)       ta.copy();
        if (e.getSource() == selectAll)  ta.selectAll();
    }

    public static void main(String[] args) {
        new MenuNpEdit();
    }
}
