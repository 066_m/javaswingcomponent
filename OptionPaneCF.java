import java.awt.event.*;
import javax.swing.*;

public class OptionPaneCF extends WindowAdapter {
    JFrame frame;

    OptionPaneCF() {
        frame = new JFrame();
        frame.addWindowListener(this);
        frame.setSize(300, 300);
        frame.setLayout(null);
        frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        frame.setVisible(true);
    }

    public void windowClosing(WindowEvent e) {
        int ask = JOptionPane.showConfirmDialog(frame, "Are you sure?");
        if (ask == JOptionPane.YES_OPTION) {
            frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        }
    }

    public static void main(String[] args) {
        new OptionPaneCF();
    }
}
