import javax.swing.*;

public class OptionPaneMsgW {
    JFrame frame;

    OptionPaneMsgW(){  
        frame = new JFrame();  
        JOptionPane.showMessageDialog(frame,"Successfully Updated.","Alert",JOptionPane.WARNING_MESSAGE);     
    }

    public static void main(String[] args) {
        new OptionPaneMsgW();
    }
}
