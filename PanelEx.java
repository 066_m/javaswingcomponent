import java.awt.*;
import javax.swing.*;

public class PanelEx {
    PanelEx() {  
        JFrame frame = new JFrame("Panel Example");    
        
        JPanel panel = new JPanel();  
        panel.setBounds(40,80,200,200);    
        panel.setBackground(Color.gray);  
        frame.add(panel); 
        
        JButton b1 = new JButton("Button 1");     
        b1.setBounds(50,100,80,30);    
        b1.setBackground(Color.yellow);   
        panel.add(b1);

        JButton b2 = new JButton("Button 2");   
        b2.setBounds(100,100,80,30);    
        b2.setBackground(Color.green);
        panel.add(b2);  
 
        frame.setSize(400,400);    
        frame.setLayout(null);    
        frame.setVisible(true);    
        }

    public static void main(String args[]) {
        new PanelEx();
    }
}
