import java.awt.event.*;
import javax.swing.*;

public class PasswordFieldEx {
    public static void main(String[] args) {
        JFrame frame = new JFrame("Password Field Example");
        
        final JLabel label = new JLabel();
        label.setBounds(20, 150, 400, 50);
        frame.add(label);

        JLabel lblU = new JLabel("Username : ");
        lblU.setBounds(20, 20, 80, 30);
        frame.add(lblU);

        final JTextField text = new JTextField();
        text.setBounds(100, 20, 100, 30);
        frame.add(text);

        JLabel lblP = new JLabel("Password : ");
        lblP.setBounds(20, 75, 80, 30);
        frame.add(lblP);

        JPasswordField value = new JPasswordField();
        value.setBounds(100, 75, 100, 30);
        frame.add(value);

        JButton btn = new JButton("Log in");
        btn.setBounds(100, 120, 80, 30);
        frame.add(btn);

        btn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String data = "Username : " + text.getText();
                data += ", Password : " + new String(value.getPassword());
                label.setText(data);
            }
        });
        
        frame.setSize(500, 500);
        frame.setLayout(null);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
    }
}
