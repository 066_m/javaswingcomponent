import java.awt.event.*;
import javax.swing.*;

public class PopupMenu {
    PopupMenu() {
        final JFrame frame = new JFrame("PopupMenu Example");

        final JLabel label = new JLabel();
        label.setHorizontalAlignment(JLabel.CENTER);
        label.setSize(400, 100);
        frame.add(label);

        final JPopupMenu popupmenu = new JPopupMenu("Edit");
        frame.add(popupmenu);

        JMenuItem cut = new JMenuItem("Cut");
        popupmenu.add(cut);
        JMenuItem copy = new JMenuItem("Copy");
        popupmenu.add(copy);
        JMenuItem paste = new JMenuItem("Paste");
        popupmenu.add(paste);

        frame.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                popupmenu.show(frame, e.getX(), e.getY());
            }
        });

        cut.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                label.setText("cut MenuItem clicked.");
            }
        });
        copy.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                label.setText("copy MenuItem clicked.");
            }
        });
        paste.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                label.setText("paste MenuItem clicked.");
            }
        });

        frame.setSize(300, 300);
        frame.setLayout(null);
        frame.setVisible(true);
    }

    public static void main(String args[]) {
        new PopupMenu();
    }
}
