import javax.swing.*;

public class ProgressBarEx extends JFrame {
    JProgressBar pb;
    int i = 0, num = 0;

    ProgressBarEx() {
        pb = new JProgressBar(0, 2000);
        pb.setBounds(40, 40, 160, 30);
        pb.setValue(0);
        pb.setStringPainted(true);
        add(pb);

        setSize(250, 150);
        setLayout(null);
    }

    public void iterate() {
        while (i <= 2000) {
            pb.setValue(i);
            i = i + 50;
            try {
                Thread.sleep(50);
            } catch (Exception e) {
            }
        }
    }

    public static void main(String[] args) {
        ProgressBarEx m = new ProgressBarEx();
        m.setVisible(true);
        m.iterate();
    }
}
