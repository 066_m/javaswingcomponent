import java.awt.event.*;
import javax.swing.*;

public class RadioButtonEx extends JFrame implements ActionListener {
    //JFrame frame;
    JRadioButton rBtn1, rBtn2;
    JButton btn;

    RadioButtonEx(){
        //frame = new JFrame();

        rBtn1 = new JRadioButton("Male");
        rBtn1.setBounds(100,50,100,30);
        add(rBtn1);

        rBtn2 = new JRadioButton("Female");
        rBtn2.setBounds(100,100,100,30);
        add(rBtn2);

        ButtonGroup bg = new ButtonGroup();
        bg.add(rBtn1);
        bg.add(rBtn2);

        btn = new JButton("click");
        btn.setBounds(100, 150, 80, 30);
        btn.addActionListener(this);
        add(btn);

        setSize(300, 300);
        setLayout(null);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if(rBtn1.isSelected()){
            JOptionPane.showMessageDialog(this,"You are Male."); 
        }
        if(rBtn2.isSelected()){
            JOptionPane.showMessageDialog(this,"You are Female."); 
        }
    }

    public static void main(String[] args) {
        new RadioButtonEx();
    }
}
