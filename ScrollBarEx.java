import java.awt.event.*;
import javax.swing.*;

public class ScrollBarEx {
    ScrollBarEx() {
        JFrame frame = new JFrame("Scrollbar Example");

        final JLabel label = new JLabel();
        label.setHorizontalAlignment(JLabel.CENTER);
        label.setSize(400, 100);
        frame.add(label);

        final JScrollBar scBar = new JScrollBar();
        scBar.setBounds(100, 100, 50, 100);
        frame.add(scBar);

        scBar.addAdjustmentListener(new AdjustmentListener() {  
            public void adjustmentValueChanged(AdjustmentEvent e) {  
               label.setText("Vertical Scrollbar value is:"+ scBar.getValue());  
            }  
        });  

        frame.setSize(400, 400);
        frame.setLayout(null);
        frame.setVisible(true);
    }

    public static void main(String args[]) {
        new ScrollBarEx();
    }
}
