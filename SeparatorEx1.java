import javax.swing.*;

public class SeparatorEx1 {

    JMenu menu, submenu;
    JMenuItem i1, i2, i3, i4, i5;

    SeparatorEx1() {
        JFrame frame = new JFrame("Separator Example");

        JMenuBar mb = new JMenuBar();

        menu = new JMenu("Menu");
       
        i1 = new JMenuItem("Item 1");
        menu.add(i1);
        
        menu.addSeparator();
        
        i2 = new JMenuItem("Item 2");
        menu.add(i2);
        
        mb.add(menu);

        frame.setJMenuBar(mb);
        frame.setSize(400, 400);
        frame.setLayout(null);
        frame.setVisible(true);
    }

    public static void main(String args[]) {
        new SeparatorEx1();
    }
}
