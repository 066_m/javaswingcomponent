import javax.swing.*;    
import java.awt.*;  

public class SeparatorEx2 {
    public static void main(String args[]) {  
        JFrame frame = new JFrame("Separator Example");      
        frame.setLayout(new GridLayout(0, 1));  
        
        JLabel lbl1 = new JLabel("Above Separator");  
        frame.add(lbl1);  
        
        JSeparator sep = new JSeparator();  
        frame.add(sep);  
       
        JLabel lbl2 = new JLabel("Below Separator");  
        frame.add(lbl2);  

        frame.setSize(400, 100);  
        frame.setVisible(true);  
      }  
}
