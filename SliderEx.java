import javax.swing.*;

public class SliderEx extends JFrame {
    public SliderEx() {
        JPanel panel = new JPanel();
        add(panel);

        JSlider slider = new JSlider(JSlider.HORIZONTAL, 0, 50, 25);
        panel.add(slider);

    }

    public static void main(String s[]) {
        SliderEx frame = new SliderEx();
        frame.pack();
        frame.setVisible(true);
    }
}
