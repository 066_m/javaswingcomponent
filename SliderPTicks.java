import javax.swing.*;

public class SliderPTicks extends JFrame {
    public SliderPTicks() {
        JPanel panel = new JPanel();
        add(panel);

        JSlider slider = new JSlider(JSlider.HORIZONTAL, 0, 50, 25);
        slider.setMinorTickSpacing(2);
        slider.setMajorTickSpacing(10);
        slider.setPaintTicks(true);
        slider.setPaintLabels(true);

        panel.add(slider);
    }

    public static void main(String s[]) {
        SliderPTicks frame = new SliderPTicks();
        frame.pack();
        frame.setVisible(true);
    }
}
