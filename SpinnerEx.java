import javax.swing.event.*;
import javax.swing.*;

public class SpinnerEx {
    public static void main(String[] args) {
        JFrame frame = new JFrame("Spinner Example");

        final JLabel label = new JLabel();
        label.setHorizontalAlignment(JLabel.CENTER);
        label.setSize(250, 100);
        frame.add(label);

        SpinnerModel value = new SpinnerNumberModel(5, // initial value
                0, // minimum value
                10, // maximum value
                1); // step

        JSpinner spinner = new JSpinner(value);
        spinner.setBounds(100, 100, 50, 30);
        frame.add(spinner);

        spinner.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                label.setText("Value : " + ((JSpinner)e.getSource()).getValue());  
            }
        });

        frame.setSize(300, 300);
        frame.setLayout(null);
        frame.setVisible(true);
    }
}
