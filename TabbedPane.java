import javax.swing.*;

public class TabbedPane {
    JFrame frame;

    TabbedPane() {
        frame = new JFrame();

        JTextArea ta = new JTextArea(200, 200);

        JTabbedPane tp = new JTabbedPane();
        tp.setBounds(50, 50, 200, 200); 
        frame.add(tp);

        JPanel p1 = new JPanel();
        p1.add(ta);
        tp.add("main", p1);

        JPanel p2 = new JPanel(); 
        tp.add("visit", p2);

        JPanel p3 = new JPanel();
        tp.add("help", p3);
       
        frame.setSize(400, 400);
        frame.setLayout(null);
        frame.setVisible(true);
    }

    public static void main(String[] args) {
        new TabbedPane();
    }
}
