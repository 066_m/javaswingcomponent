import javax.swing.event.*;
import javax.swing.*;

public class TableEx {
    public static void main(String[] args) {
        JFrame frame = new JFrame("Table Example");
        
        String data[][] = {{"101","Amit","670000"},    
                        {"102","Jai","780000"},    
                        {"101","Sachin","700000"}};
        String column[] = {"ID","NAME","SALARY"};
        
        final JTable table = new JTable(data, column);
        //table.setBounds(30, 40, 200, 300);
        table.setCellSelectionEnabled(true);

        ListSelectionModel select = table.getSelectionModel();
        select.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        select.addListSelectionListener(new ListSelectionListener(){
            @Override
            public void valueChanged(ListSelectionEvent e) {
                String Data = null;
                int[] row = table.getSelectedRows();
                int[] columns = table.getSelectedColumns();
                for(int i = 0; i < row.length; i++){
                    for(int j = 0; j < columns.length; j++){
                        Data = (String)table.getValueAt(row[i], columns[j]);  
                    }
                }
                System.out.println("Table element selected is: " + Data);
            }
        });

        JScrollPane scPane = new JScrollPane(table);
        frame.add(scPane);

        frame.setSize(500, 400);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
    }
}
