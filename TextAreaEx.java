import java.awt.event.*;
import javax.swing.*;

public class TextAreaEx implements ActionListener{
    JTextArea area;
    JLabel lbl1, lbl2;
    JButton btn;

    TextAreaEx(){
        JFrame frame = new JFrame();
        //JTextArea area = new JTextArea("Welcome to javatpoint");
        area = new JTextArea();
        area.setBounds(20, 75, 250, 200);
        frame.add(area);

        lbl1 = new JLabel();
        lbl1.setBounds(50, 25, 100, 30);
        frame.add(lbl1);

        lbl2 = new JLabel();
        lbl2.setBounds(160, 25, 100, 30);
        frame.add(lbl2);

        btn = new JButton("Count Words");
        btn.setBounds(100, 300, 120, 30);
        btn.addActionListener(this);
        frame.add(btn);

        frame.setSize(400, 400);
        frame.setLayout(null);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String text = area.getText();
        String words[] = text.split("\\s");
        lbl1.setText("Words : " + words.length);
        lbl2.setText("Characters : " + text.length());
    }

    public static void main(String[] args) {
        new TextAreaEx();
    }
    
}