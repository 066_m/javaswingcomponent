import java.awt.event.*;
import javax.swing.*;

public class TextFieldEx implements ActionListener{
    JTextField txt1, txt2, txt3;
    JFrame frame;
    JButton btn1, btn2;

    TextFieldEx(){
        frame = new JFrame("TextField Example");

        //txt1 = new JTextField("Welcome To");
        txt1 = new JTextField();
        txt1.setBounds(50, 50, 150, 20);
        frame.add(txt1);

        //txt2 = new JTextField("Dektuaklom World!");
        txt2 = new JTextField();
        txt2.setBounds(50, 100, 150, 20);
        frame.add(txt2);

        txt3 = new JTextField();
        txt3.setBounds(50, 150, 150, 20);
        txt3.setEditable(false);
        frame.add(txt3);

        btn1 = new JButton("+");
        btn1.setBounds(50, 200, 50, 50);
        btn1.addActionListener(this);
        frame.add(btn1);

        btn2 = new JButton("-");
        btn2.setBounds(120, 200, 50, 50);
        btn2.addActionListener(this);
        frame.add(btn2);

        frame.setSize(400, 400);
        frame.setLayout(null);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
    }

    public void actionPerformed(ActionEvent e){
        String str1 = txt1.getText();
        int num1 = Integer.parseInt(str1);

        String str2 = txt2.getText();
        int num2 = Integer.parseInt(str2);

        int num = 0;
        if(e.getSource() == btn1){
            num  = num1 + num2;
        } else if(e.getSource() == btn2){
            num = num1 - num2;
        }

        String result = String.valueOf(num);
        txt3.setText(result);
    }

    public static void main(String[] args) {
        new TextFieldEx();
    }
}
