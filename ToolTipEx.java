import javax.swing.*;

public class ToolTipEx {
    public static void main(String[] args) {    
        JFrame frame = new JFrame("Password Field Example");   

        JPasswordField value = new JPasswordField();   
        value.setBounds(100,100,100,30);    
        value.setToolTipText("Enter your Password");  
        frame.add(value);

        JLabel l1 = new JLabel("Password:");    
        l1.setBounds(20,100, 80,30);    
        frame.add(l1);  

        frame.setSize(300,300);    
        frame.setLayout(null);    
        frame.setVisible(true);      
   }  
}
