import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.*;

public class TreeEx {
    JFrame frame;

    TreeEx() {
        frame = new JFrame();
        DefaultMutableTreeNode style = new DefaultMutableTreeNode("Style");

        DefaultMutableTreeNode color = new DefaultMutableTreeNode("color");
        style.add(color);
        
        DefaultMutableTreeNode font = new DefaultMutableTreeNode("font");
        style.add(font);
        
        
        DefaultMutableTreeNode red = new DefaultMutableTreeNode("red");
        color.add(red);

        DefaultMutableTreeNode blue = new DefaultMutableTreeNode("blue");
        color.add(blue);

        DefaultMutableTreeNode black = new DefaultMutableTreeNode("black");
        color.add(black);

        DefaultMutableTreeNode green = new DefaultMutableTreeNode("green");
        color.add(green);
        
    
        JTree jt = new JTree(style);
        frame.add(jt);

        frame.setSize(200, 200);
        frame.setVisible(true);
    }

    public static void main(String[] args) {
        new TreeEx();
    }
}
